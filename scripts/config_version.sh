#!/bin/bash
if [ -e $1/$2/.r10k-deploy.json ]
then
  /opt/puppetlabs/puppet/bin/ruby $1/$2/scripts/config_version.rb $1 $2
else
  SERVER=$(facter hostname)
  /usr/bin/git --version > /dev/null 2>&1 &&
  HASH=$(/usr/bin/git --git-dir $1/$2/.git rev-parse HEAD) 
  echo "${SERVER}::${2}::${HASH}"
fi
