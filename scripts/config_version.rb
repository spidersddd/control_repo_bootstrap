#!/opt/puppetlabs/puppet/bin/ruby

begin
  require 'facter'
  require 'json'
rescue LoadError
  t = Time.new
  puts t.to_i
else
  environmentpath = ARGV[0]
  environment     = ARGV[1]

  def retrieve_info(r10k_deploy_name)
    if(File.file?(r10k_deploy_name))
        json_from_file = File.read(r10k_deploy_name)
        hash = JSON.parse(json_from_file)
        out = hash['signature']
    else
        out = 'unknown'
    end
    return out
  end

  # Get the hostname of the Puppet master compiling the catalog.
  # Sometimes the hostname is the fqdn, so we'll take the first segment.
  compiling_master = Facter.value('hostname')

  # Get the path to the environment being compiled.
  repo = File.join(environmentpath, environment)

  # First 12 characters of the sha1 hash of the newest commit.
  commit_id = retrieve_info("#{repo}/.r10k-deploy.json")

  # Show the compiling master, environment name, and commit ID.
  puts "#{compiling_master}::#{environment}::#{commit_id}"
end
