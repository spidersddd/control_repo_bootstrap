# role for puppetdb server
class role::config_mgmt::databaseapi_server {
  include baseline_profiles::baseline
  include puppet_infra_profiles::puppet::puppetdb
}
