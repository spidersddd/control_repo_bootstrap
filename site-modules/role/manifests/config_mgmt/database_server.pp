# role for puppet postgresql server
class role::config_mgmt::database_server {
  include baseline_profiles::baseline
  include puppet_infra_profiles::puppet::postgres_server
}
