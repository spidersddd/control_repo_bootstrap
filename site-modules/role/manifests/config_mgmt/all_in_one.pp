# Role for configuration manager server (Puppet) CA Server
class role::config_mgmt::all_in_one {
  include baseline_profiles::baseline
  include puppet_infra_profiles::puppet::postgres_server
  include puppet_infra_profiles::puppet::hiera_included
  include puppet_infra_profiles::puppet::server
}
