# A simple classification using trusted data
# in the csr_attributes.yaml you add something like
# extension_requests:
#   pp_role: config_mgmt::cert_authority
# 
# then the class 'role::config_mgmt::cert_authority'
# will be added to the host 

File {
  backup  => false,
}

if $trusted['extensions']['pp_role'] {
  include "role::${trusted['extensions']['pp_role']}"
}

