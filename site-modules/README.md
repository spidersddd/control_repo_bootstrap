# Puppet Example Roles

This directory is only a set of Roles to be used in a deployment.  The Profiles have been split apart to better facilitate the team development of profiles and to identify service owners as authorized teams when making changes to profiles.

It SHOULD go without saying that everything should pass linting/validation, but
we're gonna go ahead and say that anyway.

## Example requirements

These examples have been constructed with the following requirements:

| Supported OS | Product Role | State |
|--------------------------|---------------------|-----|
| CentOS 7 | role::config_mgmt::cert_authority | Complete |
| CentOS 7 | role::config_mgmt::database_server | Complete |
| CentOS 7 | role::config_mgmt::databaseapi_server | Complete |


  - Modeling should support three operating systems
    - CentOS (6,7)
    - Debian (10)
  - One product should be represented
    - Config_mgmt (WIP)
      - Linux systems hosting the product
  - Support services
    - While products usually do not share services, support services are used by many products and teams.
    - Example of monitoring service 'role::sup\_svc::monitoring::server'
    - Example of Puppet Master 'role::sup\_svc::storage::server'
