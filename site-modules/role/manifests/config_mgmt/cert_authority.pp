# Role for configuration manager server (Puppet) CA Server
class role::config_mgmt::cert_authority {
  include baseline_profiles::baseline
  include puppet_infra_profiles::puppet::server
}
